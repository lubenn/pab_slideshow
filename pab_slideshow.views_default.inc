<?php
/**
 * @file
 * pab_slideshow.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function pab_slideshow_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'slideshow';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'pab_slideshow';
  $view->human_name = 'Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Slideshow';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Slideshow: Heading */
  $handler->display->display_options['fields']['field_slideshow_title']['id'] = 'field_slideshow_title';
  $handler->display->display_options['fields']['field_slideshow_title']['table'] = 'field_data_field_slideshow_title';
  $handler->display->display_options['fields']['field_slideshow_title']['field'] = 'field_slideshow_title';
  $handler->display->display_options['fields']['field_slideshow_title']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_title']['alter']['text'] = '<h1 style="color:[field_slideshow_heading_color]">[field_slideshow_title]</h1>';
  $handler->display->display_options['fields']['field_slideshow_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_title']['element_default_classes'] = FALSE;
  /* Field: Slideshow: Image */
  $handler->display->display_options['fields']['field_slideshow_image']['id'] = 'field_slideshow_image';
  $handler->display->display_options['fields']['field_slideshow_image']['table'] = 'field_data_field_slideshow_image';
  $handler->display->display_options['fields']['field_slideshow_image']['field'] = 'field_slideshow_image';
  $handler->display->display_options['fields']['field_slideshow_image']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_slideshow_image']['settings'] = array(
    'image_style' => 'slideshow',
    'image_link' => '',
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_slideshow_title] ';
  $handler->display->display_options['fields']['nothing']['element_type'] = 'div';
  $handler->display->display_options['fields']['nothing']['element_class'] = 'flex-caption [field_slideshow_alignment]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  /* Sort criterion: Slideshow: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'pab_slideshow';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['slideshow'] = $view;

  return $export;
}
