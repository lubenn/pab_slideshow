<?php
/**
 * @file
 * pab_slideshow.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pab_slideshow_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'pab_slideshow-pab_slideshow-field_slideshow_image'.
  $field_instances['pab_slideshow-pab_slideshow-field_slideshow_image'] = array(
    'bundle' => 'pab_slideshow',
    'custom_add_another' => '',
    'custom_remove' => '',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pab_slideshow',
    'fences_wrapper' => 'div',
    'field_name' => 'field_slideshow_image',
    'label' => 'Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'custom_add_another' => '',
      'custom_remove' => '',
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_flexslider_full' => 0,
          'image_flexslider_thumbnail' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_slideshow' => 0,
          'image_thumbnail' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'pab_slideshow-pab_slideshow-field_slideshow_title'.
  $field_instances['pab_slideshow-pab_slideshow-field_slideshow_title'] = array(
    'bundle' => 'pab_slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Heading to show on the slide',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'pab_slideshow',
    'fences_wrapper' => 'div',
    'field_name' => 'field_slideshow_title',
    'label' => 'Heading',
    'required' => 0,
    'settings' => array(
      'custom_add_another' => '',
      'custom_remove' => '',
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Heading');
  t('Heading to show on the slide');
  t('Image');

  return $field_instances;
}
