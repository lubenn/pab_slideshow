<?php
/**
 * @file
 * pab_slideshow.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pab_slideshow_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer slideshow'.
  $permissions['administer slideshow'] = array(
    'name' => 'administer slideshow',
    'roles' => array(
      'site administrator' => 'site administrator',
    ),
    'module' => 'pab_slideshow',
  );

  return $permissions;
}
