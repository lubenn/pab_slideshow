<?php
/**
 * @file
 * pab_slideshow.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pab_slideshow_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function pab_slideshow_image_default_styles() {
  $styles = array();

  // Exported image style: slideshow.
  $styles['slideshow'] = array(
    'label' => 'Slideshow',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1574,
          'height' => 830,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
